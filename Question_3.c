#include <stdio.h>
#include <ctype.h>

int main ()
{
  char c;

  //Get inputs from user
  printf ("Enter an alphabet to check: ");
  scanf ("%c", &c);

  //Convert into lowercase
  char cLower = tolower (c);

  //Check the character
  if (cLower == 'a' || cLower == 'e' || cLower == 'i' || cLower == 'o'
      || cLower == 'u')
    {
      printf ("Entered alphabet is a VOWEL.");
    }
  else
    {
      printf ("Entered alphabet is a CONSONANT.");
    }


  return 0;
}
