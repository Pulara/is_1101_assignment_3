#include <stdio.h>

int
main ()
{
  int num;

  //Get inputs from user
  printf ("Enter a number to check: ");
  scanf ("%d", &num);

  //Check the number
  if (num % 2 == 1)
    {
      printf ("Entered number is a ODD number.");
    }
  else
    {
      printf ("Entered number is a EVEN number.");
    }


  return 0;
}

