#include <stdio.h>

int
main ()
{
  int num;

  //Get inputs from user
  printf ("Enter a number to check: ");
  scanf ("%d", &num);

  //Check the number
  if (num > 0)
    {
      printf ("Entered number is POSITIVE.");
    }
  else if (num < 0)
    {
      printf ("Entered number is NEGATIVE.");
    }
  else if (num == 0)
    {
      printf ("Entered number is ZERO.");
    }

  return 0;
}
